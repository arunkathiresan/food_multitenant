import React, { Component } from 'react'
import {EmailIcon,TwitterIcon,FacebookIcon} from "react-share";
class footer extends Component {
    render() {
        return (
            <div className="container">
                <nav className="navbar navbar-expand-sm navbar-light bg-light centre">
                    <span dangerouslySetInnerHTML={{ "__html": "&copy;" }} />
                    All rights reserved K.V foods.com 2020-2021
                    &nbsp;
                    <EmailIcon size={42} round={true} /> &nbsp;
                    <TwitterIcon size={42} round={true} /> &nbsp;
                    <FacebookIcon size={42} round={true} />
                </nav>
            </div>
        )
    }
}
export default footer;
