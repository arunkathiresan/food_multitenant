import React, { Component } from 'react'
import axios from 'axios';

class addcatagory extends Component {
    constructor(props) {
        super(props);

        this.onchangecatagoryid = this.onchangecatagoryid.bind(this);
        this.onchangecatagoryname = this.onchangecatagoryname.bind(this);

        this.onSubmit = this.onSubmit.bind(this);

        this.state = {
            id: '',
            catagory_name: '',
            catagory_error: '',
            vendorName: localStorage.getItem('vendorName'),
            submitted: false
        }
    }
    onchangecatagoryid(e) {
        this.setState({
            id: e.target.value
        });
    }
    onchangecatagoryname(e) {
        this.setState({
            catagory_name: e.target.value
        });
    }
    onSubmit(e) {
        e.preventDefault();
        const catagory = {
            id: this.state.id,
            catagory_name: this.state.catagory_name

        };
        axios.post(`http://localhost:5000/vendor/${this.state.vendorName}/catagory`, catagory)
            .then(res => {
                this.setState({
                    id: '',
                    catagory_name: '',
                    submitted: true

                })
            }).catch(err => {
                this.setState({
                    catagory_error: "Catagory Exists ",
                });
            });



    }
    cancelButton = () => {
        this.props.history.push(`/admin/vendor/${this.state.vendorName}/catagory`);
    }
    render() {

        return (

            <div >
                <h3>Create catagory</h3>
                <form onSubmit={this.onSubmit}>
                    <div className="form-group">
                        <label >Catagory ID</label>
                        <input type="number"
                            className="form-control"
                            name="id"
                            placeholder="Enter Catagory ID"
                            value={this.state.catagoryid}
                            onChange={this.onchangecatagoryid}
                        />
                        <b>Note: 101-Veg or 102-non_veg</b>
                        <div className="Error">{this.state.catagory_error}</div>

                    </div>
                    <div className="form-group">
                        <label htmlFor="Phone_number">Catagory Name</label>
                        <input type="text"
                            className="form-control"
                            name="catagory_name"
                            placeholder="Enter Catagory Name"
                            value={this.state.catagoryname}
                            onChange={this.onchangecatagoryname}
                        />
                    </div>
                    <button type="submit" className="btn btn-sm btn-success" disabled={this.state.id > 102 || this.state.id < 100}
                    >                        Add catagory
                </button>&nbsp;&nbsp;&nbsp;
                    <button type="submit" onClick={this.cancelButton} className="btn btn-sm btn-danger">
                        Cancel
              </button>
                </form>
            </div>
        )
    }
}


export default addcatagory;
