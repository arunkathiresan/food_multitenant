import React, { Component } from 'react'
import axios from 'axios';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

class forgotpassword extends Component {
    constructor(props) {
        super(props);
        this.state = {
            phone_number: '',
            phone_number_error:'',
            vendorName: localStorage.getItem('vendorname')
        }
    }
    forgotpwd(e) {
        e.preventDefault();
        const userdetail = {
            phone_number: this.refs.phone_number.value,
        };
        axios.post(`http://localhost:5000/vendor/${this.state.vendorName}/user/forgotpassword`, userdetail)
            .then(res => {
                this.setState({
                    phone_number: ''
                });
                var value=window.confirm("Your password sent your registered mobile number");
                    if(value==true){
                localStorage.clear();
                this.props.history.push(`/`);
                    }
            }
            ).catch(err => {
                this.setState({
                    phone_number_error: "Phone number Not Exists ",
                });
            });

    }
    cancelButton = () => {
        this.props.history.push(`/`);
        localStorage.clear();
    }
    render() {
        return (
            <div className="container">
                <h3>Forgot Password</h3>
                <form onSubmit={this.forgotpwd.bind(this)}>
                <ToastContainer />
                <div className="form-group">
                                <input className="form-control" ref="phone_number" title="Enter a valid phoneNumber" type="number" placeholder="phone_number" required />
                            </div>
                    <div className="Error">{this.state.phone_number_error}</div>

                    <button type="submit" className="btn btn-sm btn-success">
                        Submit
                </button>
                    &nbsp;&nbsp;&nbsp;
                    <button type="submit" onClick={this.cancelButton} className="btn btn-sm btn-danger">
                        Cancel
              </button>
                </form>
            </div>

        )
    }
}
export default forgotpassword;
