import React, { Component } from 'react'
import axios from 'axios';
import Button from 'react-bootstrap/Button'
// import { BrowserRouter as  Router,Link, NavLink } from "react-router-dom";
import { Link,NavLink } from 'react-router-dom';


import Admin from '../components/admin';

const List = props => (

    <tr>
        <td>{props.items.vendorname}</td>
        <td>
            <Link to={{ pathname: `/admin/vendor/${props.items.vendorname}/catagory` }} onClick={() => localStorage.setItem('vendorName', props.items.vendorname)}>Catagory</Link>
        </td>
       
        <td>
            <Link to={{ pathname: `/admin/vendor/${props.items.vendorname}/food` }} onClick={() => localStorage.setItem('vendorName', props.items.vendorname)}>Food</Link>
        </td>
        <td>
            <Button variant="danger" size="sm" onClick={e => deleteVendor(props.items.vendorname)}>Delete Vendor</Button>
        </td>
    </tr>
)



function deleteVendor(e) {
    console.log(e);
      axios.delete('http://localhost:5000/vendor/', { params: { vendor: e } })
        .then(response => {
            console.log("deleted successfully");

        })
        .catch(function (error) {
            console.log(error);
        })
}


class adminvendor extends Component {
    constructor(props) {
        super(props);
        this.state = { items: [] };

    }
    componentDidMount() {
        axios.get('http://localhost:5000/vendor')
            .then(response => {
                this.setState({ items: response.data });

            })
            .catch(function (error) {
                console.log(error);
            })

    }

    showVendor() {
        return this.state.items.map(function (currentList, i) {
            return <List items={currentList} key={i} />;
        })
    }


    render() {
        return (
            <div>
                <Admin />
                <h3>Vendor Details</h3>
                <NavLink to="/admin/vendor/addvendor" activeStyle={{
                    fontWeight: "bold",
                    color: "green",
                    align: "right"
                }} className="nav-link">CreateVendor</NavLink>
                <table className="table table-striped" style={{ marginTop: 20 }} >
                    <thead>
                        <tr>
                            <th>Vendor Name</th>
                            <th>catagory</th>
                            <th>Food</th>
                            <th>Delete</th>
                        </tr>
                    </thead>
                    <tbody>
                        {this.showVendor()}
                    </tbody>
                </table>
            </div>
        )
    }
}

export default adminvendor;
