import React, { Component } from 'react'
import axios from 'axios';

class updatecatagory extends Component {
    constructor(props) {
        super(props);

        this.onchangecatagoryid = this.onchangecatagoryid.bind(this);
        this.onchangecatagoryname = this.onchangecatagoryname.bind(this);
        this.onSubmit = this.onSubmit.bind(this);
        this.state = {
            id: '',
            catagory_name: '',
            vendorName:localStorage.getItem('vendorName')
        }
    }
    componentDidMount() {
        axios.get(`http://localhost:5000/vendor/${this.state.vendorName}/catagory/${this.props.match.params.id}`)
            .then(response => {
                console.log(response.data);
                this.setState({
                    id: response.data.id,
                    catagory_name: response.data.catagory_name,
                        })
            })
            .catch(function (error) {
                console.log(error);
            })
    }
    onchangecatagoryid(e) {
        this.setState({
            id: e.target.value
        });
    }
    onchangecatagoryname(e) {
        this.setState({
            catagory_name: e.target.value
        });
    }
    onSubmit(e) {
        e.preventDefault();
        const catagory = {
            id: this.state.id,
            catagory_name: this.state.catagory_name

        };
console.log(this.props.id);
        axios.put(`http://localhost:5000/vendor/${this.state.vendorName}/catagory/`,+this.state.id, catagory)
            .then(res => console.log(res.data));

        this.setState({
            id: '',
            catagory_name: ''
        });
    }
    render() {
        return (
            <div>
                <form onSubmit={this.onSubmit}>
                    <div className="form-group">
                        <label >Catagory ID</label>
                        <input type="number"
                            className="form-control"
                            name="id"
                            placeholder="Enter Catagory ID"
                            value={this.state.id}
                            onChange={this.onchangecatagoryid}
                        />
                    </div>
                    <div className="form-group">
                        <label htmlFor="Phone_number">Catagory Name</label>
                        <input type="text"
                            className="form-control"
                            name="catagory_name"
                            placeholder="Enter Catagory Name"
                            value={this.state.catagoryname}
                            onChange={this.onchangecatagoryname}
                        />
                    </div>
                    <button type="submit" className="btn btn-sm btn-success">
                        Update Catagory
            </button>
                </form>
            </div>
        )
    }
}
export default updatecatagory;
