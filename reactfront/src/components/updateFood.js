import React, { Component } from 'react'
import axios from 'axios';

class updateFood extends Component {
    constructor(props) {
        super(props);
        this.onchangecatagory_id = this.onchangecatagory_id.bind(this);
        this.onchangefood_name = this.onchangefood_name.bind(this);
        this.onchangefood_price = this.onchangefood_price.bind(this);
        this.onSubmit = this.onSubmit.bind(this);
        this.state = {
            catagory_id: '',
            food_name: '',
            food_price: '',
            vendorName: localStorage.getItem('vendorName')
        }
    }
    onchangecatagory_id(e) {
        this.setState({
            catagory_id: e.target.value
        });
    }
    onchangefood_name(e) {
        this.setState({
            food_name: e.target.value
        });
    }
    onchangefood_price(e) {
        this.setState({
            food_price: e.target.value
        });
    }
    componentDidMount() {
        this.getFood();
    }
    getFood = (e) => {
        axios.get(`http://localhost:5000/vendor/${this.state.vendorName}/food/${this.props.match.params.id}`)
            .then(response => {
                this.setState({
                    catagory_id:  response.data[0].catagory_id,
                    food_name: response.data[0].food_name,
                    food_price: response.data[0].food_price
                });
            })
            .catch(function (error) {
                console.log(error);
            })
    }
    onSubmit(e) {
        e.preventDefault();
        const food = {
            catagory_id: this.state.catagory_id,
            food_name: this.state.food_name,
            food_price: this.state.food_price
        };
        axios.put(`http://localhost:5000/vendor/${this.state.vendorName}/food/${this.props.match.params.id}`,food)
            .then(res => console.log(res.data));
        this.setState({
            catagory_id: '',
            food_name: '',
            food_price: ''
        });
    }
    cancelButton = () => {
        this.props.history.push(`/admin/vendor/${this.state.vendorName}/food`);
    }
    render() {
        return (
            <div >
                <h3>Update Food</h3>
                <form onSubmit={this.onSubmit}>
                    <div className="form-group">
                        <label >Catagory ID </label>
                        <input type="number"
                            className="form-control"
                            name="catagory_id"
                            placeholder="Entercatagory Id"
                            value={this.state.catagory_id}
                            onChange={this.onchangecatagory_id}
                            disabled
                        />
                        <b>Note: 101-Veg or 102-non_veg</b>
                    </div>
                    <div className="form-group">
                        <label htmlFor="Phone_number">Food name</label>
                        <input type="text"
                            className="form-control"
                            name="food_name"
                            placeholder="Enter food Name"
                            value={this.state.food_name}
                            onChange={this.onchangefood_name}
                        />

                    </div>
                    <div className="form-group">
                        <label htmlFor="Phone_number">Food price</label>
                        <input type="number"
                            className="form-control"
                            name="food_price"
                            placeholder="Enter foodprice"
                            value={this.state.food_price}
                            onChange={this.onchangefood_price}
                        />
                    </div>
                    <button type="submit" className="btn btn-sm btn-success" disabled={this.state.catagory_id > 102 || this.state.catagory_id < 100}
                    >
                        Add food
                </button>
                    &nbsp;&nbsp;&nbsp;
                    <button type="submit" onClick={this.cancelButton} className="btn btn-sm btn-danger">
                        Cancel
              </button>
                </form>
            </div>
        )
    }
}
export default updateFood
