import React, { Component } from 'react'
import {NavLink } from 'react-router-dom';

class vendorHeader extends Component {
    constructor() {
        super();
        this.state = {
            vendorname: localStorage.getItem('vendorname')
        }
    }
    render() {
        return (
            <div className="container">
                <nav className="navbar navbar-expand-sm navbar-light bg-light">
                    <div>WELCOME TO <b>{this.state.vendorname}</b></div>&nbsp;
                   <NavLink to={{ pathname: `/` }} 
                    activeStyle={{
                        fontWeight: "bold",
                        color: "red",
                    }} onClick={() => localStorage.clear()}>HOME</NavLink>
                </nav>
            </div>
        )
    }
}
export default vendorHeader;
