import React, { Component } from 'react'
import axios from 'axios';
import Button from 'react-bootstrap/Button'
import '../css/vendor.css'
import '../css/vendor.css'
class vendor extends Component {
    constructor(props) {
        super(props);
        this.state = { items: [] };
    }
    componentDidMount() {
        this.getVendor();
    }
    getVendor() {
        axios.get('http://localhost:5000/vendor')
            .then(response => {
                console.log(response.data);
                this.setState({ items: response.data });

            })
            .catch(function (error) {
                console.log(error);
            })
    }
    dashboard(e) {
        localStorage.setItem('vendorname', e.target.value);
        this.props.history.push("/login");

    }
    render() {

        return (
            <div className="container1">
                <h1>V_E_N_D_O_R_S</h1>
                <br />
                {this.state.items.map(food => (
                    <Button className="vendorbutton" size="lg" onClick={this.dashboard.bind(this)} value={food.vendorname}>{food.vendorname}</Button>
                ))}

            </div>
        )
    }
}
export default vendor
