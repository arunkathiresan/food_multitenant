import React, { Component } from 'react'

class admin extends Component {
    
    cancelButton = () => {
    localStorage.clear();        
    }
    render() {
        return (
            <div>
                <h1>Welcome Admin</h1>
                <div>
                    {(() => {
                        if (localStorage.getItem('value') == 1) {
                            return (
                                <button type="submit" onClick={this.cancelButton} className="btn btn-sm btn-danger">
                                    Logout
              </button>)
                        }
                    })()}
                </div>
            </div>
        )
    }
}
export default admin;
