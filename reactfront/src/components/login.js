import React, { Component } from 'react'
import axios from 'axios';
import { Link,Redirect } from "react-router-dom";
import validator from 'validator';
import VendorHeader from './vendorHeader';


class login extends Component {
    constructor() {
        super()
        this.state = {
            phone_number: '',
            password: '',
            phone_number_error: '',
            password_error: '',
            validation: false,
            vendorname: localStorage.getItem('vendorname'),
        }
    }
    logIn(event) {
        event.preventDefault()
        const user = {
            phone_number: this.refs.phone_number.value,
            password: this.refs.password.value,
        };
        let error = [];
        if (validator.isAlphanumeric(user.password)) {
            this.setState({
                password: user.password,
                password_error: '',
            });
            error.push(1);
        } else {
            this.setState({
                password_error: 'No special characters allowed',
            });
            error.push(0);
        }
        if (!error.includes(0)) {
            if (user.phone_number == 9865097537) {
                axios.post(`http://localhost:5000/admin/login`, user)
                    .then(res =>
                        this.props.history.push(`/admin/vendor`));
                localStorage.setItem('value', 1);
            } else {
                axios.post(`http://localhost:5000/vendor/${this.state.vendorname}/user/login`, user)
                    .then(res => {
                        this.props.history.push(`/vendor/${this.state.vendorname}/food`)
                        localStorage.setItem('value', 0);
                    })
                    .catch(err => {
                        this.setState({
                          phone_number_error: "Not valid user ",
                          validation: false
                        });
                      });
            }
        }
    }
    render() {
        // if (this.state.validation) {
        //     console.log("ahi");
        //     return (<Redirect exact to={`/vendor/${localStorage.getItem('vendorname')}/food` }/>)
        //   }
        return (
            <div className="container">
                <VendorHeader/>
                <div className="row">
                    <div className="col-md-6 mt-5 mx-auto">
                        <form onSubmit={this.logIn.bind(this)}>
                            <div className="Error"> {this.state.errorMessage} </div>
                            <label>Phone_Number</label>
                            <div className="form-group">
                                <input className="form-control" ref="phone_number" title="Enter a valid phoneNumber" type="number" placeholder="phone_number" required />
                            </div>
                            <div className="Error" >{this.state.phone_number_error}</div>
                            <label>Password</label>
                            <div className="form-group">
                                <input className="form-control" title="Enter a valid password" maxLength="10" ref="password" type="password" placeholder="Password" required />
                            </div>
                            <div className="Error">{this.state.password_error}</div>

                            <div className="input-group">
                                <button type="submit" className="btn btn-success btn-md Submit">Submit</button>
                            </div>
                            <div className="Navigation">
                                New User? <Link to="/signup" exact="true" >Register</Link>
                            </div>
                            <div className="Navigation">
                                 <Link to="/forgotpassword" exact="true" >Forgot Password</Link>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        )
    }
}

export default login;

