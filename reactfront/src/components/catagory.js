import React, { Component } from 'react'
import axios from 'axios';
import Button from 'react-bootstrap/Button'

const List = props => (
    <div>
        <Button variant="success" size="lg" block>{props.items.catagory_name}</Button><br />
    </div>
)

class catagory extends Component {
    constructor(props) {
        super(props);
        this.state = { items: [] };

    }
    componentDidMount() {
        axios.get('http://localhost:5000/vendor/swiggy/catagory')
            .then(response => {
                console.log(response.data);
                this.setState({ items: response.data });

            })
            .catch(function (error) {
                console.log(error);
            })
    }
    showVendor() {
        return this.state.items.map(function (currentList, i) {
            return <List items={currentList} key={i} />;
        })
    } render() {
        return (
            <div>
                <h1>Catagory</h1>
                {this.showVendor()}
            </div>
        )
    }
}
export default catagory;

