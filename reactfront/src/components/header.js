import React, { Component } from 'react'
import logo from '../logo.png'

class header extends Component {
    render() {
        return (
            <div className="container">
                <nav className="navbar navbar-expand-sm navbar-light bg-light">
                    <img src={logo} width="80" height="60" alt="hotel logo" />
                    <h2>K.V Foods</h2>
                </nav>
            </div>
        )
    }
}
export default header;
