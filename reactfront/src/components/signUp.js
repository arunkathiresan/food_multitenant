import React, { Component } from 'react';
import { Link, Redirect } from "react-router-dom";
import axios from 'axios';
import validator from 'validator';
import '../css/signup.css'
import VendorHeader from './vendorHeader';

class Signup extends Component {
  constructor() {
    super();
    this.state = {
      phone_number: '',
      password: '',
      conformPassword: '',
      phone_number_error: '',
      password_error: '',
      confirmPassword_error: '',
      validation: false,
      vendorname: localStorage.getItem('vendorname'),
    }
  }
  signUp(event) {
    event.preventDefault();
    let password = this.refs.password.value;
    let confirmPassword = this.refs.confirmPassword.value;
    let error = [];

    if (validator.isAlphanumeric(password)) {
      this.setState({
        password: password,
        password_error: '',
      });
      error.push(1);
    } else {
      this.setState({
        password_error: 'No special characters allowed',
      });
      error.push(0);
    }
    if (password === confirmPassword) {
      this.setState({
        confirmPassword: confirmPassword,
        confirmPassword_error: '',
      });
      error.push(1);
    } else {
      this.setState({
        confirmPassword_error: 'Password should be same',
      });
      error.push(0);
    }
    if (!error.includes(0)) {
      const user1 = {
        phone_number: this.refs.phone_number.value,
        password: this.refs.password.value
      };
      axios.post(`http://localhost:5000/vendor/${this.state.vendorname}/user`, user1)

        .then(result => {
          axios.post(`http://localhost:5000/vendor/${this.state.vendorname}/user/login`, user1)
            .then(result => {
              localStorage.setItem('value', 0);
              this.setState({
                validation: true,
                errorMessage: '',
              });
            });
        }).catch(err => {
          this.setState({
            phone_number_error: "Phone_number Exists ",
            validation: false
          });
        });
    }
  }
  render() {
    if (this.state.validation) {
      return (<Redirect exact to="/vendor/:vendorname/food" />)
    }
    return (
      <div className="form-block  SingupStyle">
              <VendorHeader/>
        <form onSubmit={this.signUp.bind(this)}>
          <label>Phone_Number</label>
          <div className="input-group Data">
            <input className="form-control" ref="phone_number" maxLength="10" title="Enter a valid phone_number" type="number" placeholder="Phone_Number" onChange={event => this.setState({ phone_number: event.target.value })} required />
          </div>
          <div className="Error" >{this.state.phone_number_error}</div>
          <label>Password</label>
          <div className="input-group Data">
            <input className="form-control" maxLength="10" ref="password" title="Password should be Alphanumeric with maxlength 10" type="password" placeholder="Password" onChange={event => this.setState({ password: event.target.value })} required />
          </div>
          <div className="Error">{this.state.password_error}</div>
          <label>Re-Enter Password</label>
          <div className="input-group Data">
            <input className="form-control" maxLength="10" ref="confirmPassword" title="Password should be Alphanumeric with maxlength 10" type="password" placeholder="Confirm Password" onChange={event => this.setState({ confirmPassword: event.target.value })} required />
          </div>
          <div className="Error">{this.state.confirmPassword_error}</div>
          <br />
          <div className="input-group">
            <button type="submit" className="btn btn-success btn-md Submit">Submit</button>
            <button type="reset" className="btn btn-warning btn-md Cancel">Reset</button>
          </div>
          <div className="Navigation">
            Have an Account? <Link to="/login" exact="true" >Login</Link>
          </div>
        </form>
      </div>
    );
  }
}
export default Signup;