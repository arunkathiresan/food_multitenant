import React, { Component } from 'react';
import Button from 'react-bootstrap/Button'
import axios from 'axios';
import Logout from './logout';

class food extends Component {
    constructor(props) {
        super(props);
        this.state = {
            items: [],
            vendorname: localStorage.getItem('vendorname'),
        };
    }
    componentDidMount() {
        axios.get(`http://localhost:5000/vendor/${this.state.vendorname}/food`)
            .then(response => {
                console.log(response.data);
                this.setState({ items: response.data });
            })
            .catch(function (error) {
                console.log(error);
            })
    }
    vegCatagory = () => {
        axios.get(`http://localhost:5000/vendor/${this.state.vendorname}/food/catagory/101`)
            .then(response => {
                console.log(response.data);
                this.setState({ items: response.data });
            })
            .catch(function (error) {
                console.log(error);
            })
    }
    nonVegCatagory = () => {
        axios.get(`http://localhost:5000/vendor/${this.state.vendorname}/food/catagory/102`)
            .then(response => {
                console.log(response.data);
                this.setState({ items: response.data });
            })
            .catch(function (error) {
                console.log(error);
            })
    }
    allCatagory = () => {
        axios.get(`http://localhost:5000/vendor/${this.state.vendorname}/food`)
            .then(response => {
                console.log(response.data);
                this.setState({ items: response.data });
            })
            .catch(function (error) {
                console.log(error);
            })
    }

    render() {
        return (
            <div>
                <h3>Food Details</h3>
                <Logout/>
                <Button variant="danger" style={{ display: 'flex', justifyContent: 'right' }} className="float-right" onClick={e => this.nonVegCatagory()}>Non Veg</Button>
                <Button variant="success" style={{ display: 'flex', justifyContent: 'right' }} className="float-right" onClick={e => this.vegCatagory()}>Veg</Button>
                <Button tyle={{ display: 'flex', justifyContent: 'right' }} className="float-right" onClick={e => this.allCatagory()}>All</Button>

                <table className="table table-striped" style={{ marginTop: 20 }} >
                    <thead>
                        <tr>
                            <th>Food Name</th>
                            <th>Price</th>
                            <th>Add To Cart</th>
                        </tr>
                    </thead>
                    <tbody>
                        {this.state.items.map(food => (
                            <tr key={food.id}>
                                <td>{food.food_name}</td>
                                <td>{food.food_price}</td>
                                <td><Button variant="success" size="sm">Add To Cart</Button></td>
                            </tr>
                        ))}
                    </tbody>
                </table>
            </div>
        )
    }
}
export default food
