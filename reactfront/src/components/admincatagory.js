import React, { Component } from 'react'
import axios from 'axios';
import { Link } from 'react-router-dom';
import Button from 'react-bootstrap/Button'
import Admin from '../components/admin'

class admincatagory extends Component {
    constructor(props) {
        super(props);
        this.state = {
            items: [],
            vendorName: localStorage.getItem('vendorName'),
            fooddelete: false
        };
    }
    componentDidMount() {
        this.getCategory()
    }
    getCategory = (e) => {
        axios.get(`http://localhost:5000/vendor/${this.state.vendorName}/catagory`)
            .then(response => {
                this.setState({ items: response.data });

            })
            .catch(function (error) {
                console.log(error);
            })
    }
    deleteCatagory = (e) => {
        console.log(e);
        var value=window.confirm();
    if(value===true){
        axios.delete(`http://localhost:5000/vendor/${localStorage.getItem('vendorName')}/catagory/${e}`)
            .then(response => {
                this.setState({
                    fooddelete: true
                }, this.getCategory())
            })
    }}
    render() {
        return (
            <div>
                <Admin />
                <h3>catagory Details</h3>
                <Link to={{ pathname: `/admin/vendor/` }} > Home</Link>
                &nbsp;&nbsp;&nbsp;&nbsp;
                <Link to={{ pathname: `/admin/vendor/${this.state.vendorName}/catagory/addcatagory` }} > Add Catagory</Link>
                &nbsp;&nbsp;&nbsp;&nbsp;
                <Link to={{ pathname: `/admin/vendor/${this.state.vendorName}/food` }} > Food</Link>
                <table className="table table-striped" style={{ marginTop: 20 }} >
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Catagory_Name</th>
                            {/* <th>Edit</th> */}
                            <th>Delete</th>
                        </tr>
                    </thead>
                    <tbody>
                        {this.state.items.map(vendor => (
                            <tr key={vendor.id}>
                                <td>{vendor.id}</td>
                                <td>{vendor.catagory_name}</td>
                                {/* <td><Link to={{ pathname: `/admin/vendor/${localStorage.getItem('vendorName')}/catagory/` + vendor.id }}>Edit</Link></td> */}
                                <td><Button variant="danger" size="sm" onClick={e => this.deleteCatagory(vendor.id)}>Delete</Button></td>
                            </tr>
                        ))}
                    </tbody>
                </table>
            </div>
        )
    }
}
export default admincatagory;
