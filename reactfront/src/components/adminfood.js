import React, { Component } from 'react'
import axios from 'axios';
import { Link } from 'react-router-dom';
import Button from 'react-bootstrap/Button'
import Admin from '../components/admin'

class adminfood extends Component {
    constructor(props) {
        super(props);
        this.state = {
            items: [],
            vendorName: localStorage.getItem('vendorName'),
            fooddelete: false
        };
    }
    componentDidMount() {
        this.getFood();
    }
    getFood = (e) => {
        axios.get(`http://localhost:5000/vendor/${this.state.vendorName}/food`)
            .then(response => {
                this.setState({ items: response.data });

            })
            .catch(function (error) {
                console.log(error);
            })
    }
    deleteFood = (e) => {
        var value=window.confirm("Are you sure delete this food");
        if(value===true){
        axios.delete(`http://localhost:5000/vendor/${localStorage.getItem('vendorName')}/food/${e}`)
            .then(response => {
                this.setState({
                    fooddelete: true
                }, this.getFood())
            })
    }}
    render() {
        return (
            <div>
                <Admin />
                <h3>Food Details</h3>
                <Link to={{ pathname: `/admin/vendor/` }} > Home</Link>
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <Link to={{ pathname: `/admin/vendor/${this.state.vendorName}/food/addfood` }} > Add Food</Link>
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <Link to={{ pathname: `/admin/vendor/${this.state.vendorName}/catagory/` }} > Catagory Home</Link>
                <table className="table table-striped" style={{ marginTop: 20 }} >
                    <thead>
                        <tr>
                            <th>Food Name</th>
                            <th>Price</th>
                            <th>Edit</th>
                            <th>Delete</th>
                        </tr>
                    </thead>
                    <tbody>
                        {this.state.items.map(food => (
                            <tr key={food.id}>
                                <td>{food.food_name}</td>
                                <td>{food.food_price}</td>
                                <td><Link to={{ pathname: `/admin/vendor/${localStorage.getItem('vendorName')}/food/edit/${food.id}`}} id={food.id}>Edit</Link></td>
                                <td><Button variant="danger" size="sm" onClick={e => this.deleteFood(food.id)}>Delete</Button></td>
                            </tr>
                        ))}
                    </tbody>
                </table>
            </div>
        )
    }
}
export default adminfood;
