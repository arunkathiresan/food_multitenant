import React, { Component } from 'react'
import axios from 'axios';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

class addvendor extends Component {
    constructor(props) {
        super(props);
        this.onchangevendorname = this.onchangevendorname.bind(this);
        this.onSubmit = this.onSubmit.bind(this);
        this.state = {
            vendorname: ''
        }
    }
    onchangevendorname(e) {
        this.setState({
            vendorname: e.target.value
        });
    }
    onSubmit(e) {
        e.preventDefault();
        const h1 = {
            vendorname: this.state.vendorname,
        };

        axios.post('http://localhost:5000/vendor/', null, { params: { h1 } })
            .then(res => console.log(res.data)
            );
                this.setState({
            vendorname: '',
        });
    }
    cancelButton = () => {
        this.props.history.push(`/admin/vendor`);
    }
    render() {
        return (
            <div >
                <ToastContainer />
                <h3>Create vendor</h3>
                <form onSubmit={this.onSubmit}>
                    <div className="form-group">
                        <label htmlFor="Phone_number">Vendor Name</label>
                        <input type="text"
                            className="form-control"
                            name="venodrname"
                            placeholder="Enter Vendor Name"
                            value={this.state.vendorname}
                            onChange={this.onchangevendorname}
                            required
                            minLength="5" maxLength="15"
                        />
                    </div>
                    <button type="submit" className="btn btn-sm btn-success">
                        Add vendor
                </button>
                    <button type="submit" onClick={this.cancelButton} className="btn btn-sm btn-danger">
                        Cancel
              </button>
                </form>
            </div>
        )
    }
}
export default addvendor;
