import React from 'react';
import { Route, Switch, Redirect } from 'react-router-dom';
import Vendor from '../components/vendor';
import Signup from '../components/signUp'
import Login from '../components/login'
import Catagory from '../components/catagory';
import Food from '../components/food'
import Admin from '../components/admin'
import Adminvendor from '../components/adminvendor'
import Adminfood from '../components/adminfood'
import Admincatagory from '../components/admincatagory'
import Addvendor from '../components/addvendor'
import Addcatagory from '../components/addcatagory'
import Addfood from '../components/addfood';
import UpdateFood from '../components/updateFood';
import Forgotpassword from '../components/forgotpassword'
function RouterComponent() {
    const AdminRoute = ({ component: Component, ...rest }) => (
        <Route {...rest} render={(props) => (
           localStorage.getItem("value") === "1" ? <Component {...props} />
            : <Redirect to='/login' />
        )} />
      )
    
    //   const UserRoute = ({ component: Component, ...rest }) => (
    //     <Route {...rest} render={(props) => (
    //       localStorage.getItem("value") === "0" ?
    //         <Component {...props} />
    //         : <Redirect to='/login' />
    //     )} />
    //   )
    return (
        <div className="container">
            <Switch>
                <Route path="/" exact component={Vendor} />
                <Route path="/admin" exact component={Admin} />
                <AdminRoute path="/admin/vendor" exact component={Adminvendor} />
                <Route path="/signup" exact component={Signup} />
                <Route path="/login" exact component={Login} />
                <Route path="/vendor/swiggy/catagory" exact component={Catagory} />
                <Route path="/vendor/:vendorname/food" exact component={Food} />
                <Route path="/forgotpassword" exact component={Forgotpassword}/>
                <AdminRoute path="/admin/vendor/:vendorname/food" exact component={Adminfood}/>
                <AdminRoute path="/admin/vendor/:vendorname/catagory" exact component={Admincatagory} />
                <AdminRoute path="/admin/vendor/addvendor" exact component={Addvendor} />
                <AdminRoute path="/admin/vendor/:vendorname/catagory/addcatagory" exact component={Addcatagory} />
                <AdminRoute path="/admin/vendor/:vendorname/food/addfood" exact component={Addfood} />
                <AdminRoute path="/admin/vendor/:vendorname/food/edit/:id" exact component={UpdateFood} />
            </Switch>
        </div>
    )
}
export default RouterComponent; 
