import React from 'react';
import "bootstrap/dist/css/bootstrap.min.css";
import Header from './components/header';
import {BrowserRouter} from 'react-router-dom';
import Carousel from './components/carosual'
import RouterComponent from '../src/Routers/router'
import 'pure-react-carousel/dist/react-carousel.es.css';
import { Container, Row, Col } from 'reactstrap';
import Footer from '../src/components/footer'

function App() {               
    return (
      <BrowserRouter >
      <React.Fragment>
         <Header /> 
         <br/>
         <Container>
         <Row xs="2">
         <Col> <Carousel/></Col>
         <Col>
         <RouterComponent /> 
         </Col>
         </Row>
         </Container>
         <Footer/>
       </React.Fragment>
   </BrowserRouter>
  );
}

export default App;
