const express=require('express');
const bodyParser=require('body-parser');
const cors=require("cors")
const app=express();
const port=5000;
require(`./controller/db`)
const rts = require(`./routes/router`);
const ats=require(`./routes/adminrouter`)

app.use(cors());

//body parser
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({
    extended:true,
}))

app.use(`/vendor`,rts);
app.use(`/`,ats);

app.get('/',(req,res)=>{
res.json('Food management Multitent').status(200);
})

//Listening post
app.listen(port,()=>{
    console.log(`App running on the port ${port}`);
})