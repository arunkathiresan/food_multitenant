const express = require('express')
const router = express.Router()
const mydb = require(`../controller/vendor.controller`)
const userdb=require(`../controller/user.controller`)
const catagorydb=require(`../controller/catagory.controller`)
const fooddb=require(`../controller/food.controller`)
const cartdb=require(`../controller/cart.controller`)

router.get('/',mydb.vendorList);
router.post('/', mydb.createVendor);
router.delete('/',mydb.deleteVendor);

router.post('/:vendor/user',userdb.createUser);
router.get('/:vendor/user',userdb.userList);
router.post('/:vendor/user/login',userdb.loginUser);
router.put('/:vendor/user/:id',userdb.updateUser);
router.delete('/:vendor/user/:id',userdb.deleteUser);
router.post('/:vendor/user/forgotpassword',userdb.forgotpassword);

router.post('/:vendor/catagory',catagorydb.addCatagory);
router.get('/:vendor/catagory',catagorydb.listCatagory);
router.get('/:vendor/catagory/:id',catagorydb.listSingleCatagory);
router.put('/:vendor/catagory/:id',catagorydb.updateCatagory);
router.delete('/:vendor/catagory/:id',catagorydb.deleteCatagory);

router.post('/:vendor/food',fooddb.addFood);
router.get('/:vendor/food',fooddb.listFood);
router.get('/:vendor/food/catagory/:catagory_id',fooddb.listCatagoryFood);
router.get('/:vendor/food/:id',fooddb.listFoodId);
router.put('/:vendor/food/:id',fooddb.updateFood);
router.delete('/:vendor/food/:id',fooddb.deleteFood);

router.post('/:vendor/cart',cartdb.addToCart);
router.get('/:vendor/cart',cartdb.listCart);
router.put('/:vendor/cart/:id',cartdb.updateCartItem);
router.delete('/:vendor/cart/:id',cartdb.deleteCartItem);



module.exports = router
