const Pool=require('pg').Pool

//new connection
const pool=new Pool({
    user:'postgres',
    host:'localhost',
    database:'hotel',
    password:'food',
    port:'5432'
})

//DB connection status
pool.connect()
.then(()=>console.log(`connection succeed`))
.catch(()=>console.log(`error in connecting check the URI`))

module.exports=pool
