const pool = require('./db')
const accountSid = 'ACfdc73e5aa7f4a48ea10a87d16015f603';
const authToken = '1fa5d82276633d60b2f5e56a33cce0b0';
const client = require('twilio')(accountSid, authToken);

createUser = (req, res) => {
    const vendorName = req.params.vendor;
    const phone_number = req.body.phone_number;
    const password = req.body.password;
    pool.query(`(select phone_number from ${vendorName}.userDetail where phone_number=${phone_number});`, (error, results) => {
        if (results.rowCount == 0) {
            pool.query(`INSERT INTO ${vendorName}.userDetail(phone_number,password) VALUES (${phone_number},'${password}');`, (error, results) => {
                if (error) {
                    console.log(error);
                    res.status(500).json({
                        message: "Data not inserted",
                        error: error
                    })
                } else {
                    res.status(200).json({
                        message: "success Register",
                        error: error
                    })
                }
            })
        }
        else {
            res.status(500).json(`user already exists`)
        }
    }
    )

}
loginUser = (req, res) => {
    const vendorName = req.params.vendor;
    const phone_number = req.body.phone_number;
    const password = req.body.password;
    pool.query(`(select phone_number,password from ${vendorName}.userDetail where phone_number=${phone_number} and password='${password}');`, (error, results) => {
        if (error) {
            console.log(error);
            res.status(500).json({
                message: "Invalid Login",
                error: error
            })
        } else {
            if (results.rowCount == 1) {
                res.status(200).json(`login success`);
            } else {
                console.log(error);
                res.status(500).json({
                    message: "Invalid Login",
                    error: error
                })
            }
        }
    })
}
forgotpassword = (req, res) => {
    const vendorName = req.params.vendor;
    const phone_number = req.body.phone_number;
    pool.query(`(select phone_number from ${vendorName}.userDetail where phone_number=${phone_number});`, (error, results) => {
        if (error) {
            console.log(error);
            res.status(500).json({
                message: "user not exists",
                error: error
            })
        } else {
            if (results.rowCount == 1) {
                pool.query(`SELECT password FROM ${vendorName}.userDetail where phone_number=${phone_number} `, (error, results) => {
                    const a1 = results.rows[0].password;
                    const a2 = '+91' + phone_number;
                    client.messages
                        .create({
                            body: " Your "+vendorName+" user login password: " + a1,
                            from: '+18707255163',
                            to: a2
                        }).then(message => console.log(message.sid))
                        

                })
                res.status(200).json(`login success`);
            }
            else {
                console.log(error);
                res.status(500).json({
                    message: "User not exits",
                    error: error
                })
            }
        }
    })

}
userList = (req, res) => {
    const vendorName = req.params.vendor;
    pool.query(`SELECT id,phone_number FROM ${vendorName}.userDetail`, (error, results) => {
        if (error) {
            res.status(204).json({
                message: "Nothing to display",
                error: error
            })
        } else {
            res.status(200).json(results.rows)
        }
    })
}
updateUser = (req, res) => {
    const vendorName = req.params.vendor;
    const id = req.params.id;
    const newpassword = req.body.newpassword;
    console.log(newpassword)
    pool.query(`UPDATE ${vendorName}.userDetail SET password='${newpassword}' where id=${id} `, (error, results) => {
        if (error) {
            console.log(error)
            res.status(500).json({
                message: "data not updated",
                error: error
            })
        } else {
            res.status(200).json("data updated")
        }
    });
}
deleteUser = (req, res) => {
    const vendorName = req.params.vendor;
    const id = req.params.id;
    console.log("deleteing");
    pool.query(`Delete from ${vendorName}.userDetail where id=${id} `, (error, results) => {
        if (error) {
            console.log(error)
            res.status(500).json({
                message: "data not deleted",
                error: error
            })
        } else {
            res.status(200).json("data deleted")
        }
    });
}

module.exports = {
    createUser,
    userList,
    updateUser,
    deleteUser,
    loginUser,
    forgotpassword
}