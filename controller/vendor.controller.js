const pool = require('./db')


createVendor = (req, res) => {
    const vendorName = JSON.parse(req.query.h1).vendorname;
        pool.query(`CREATE SCHEMA IF NOT EXISTS ${vendorName}`, (error, results) => {
        if (error) {
            res.status(500).json({
                message: "Schema Not created",
                error: error
            })
            throw error
        } else {
            res.status(201).json(`schema for ${vendorName} created sucessfully`)
            const userDetail = `CREATE TABLE ${vendorName}.userDetail(id serial PRIMARY KEY, phone_number bigint,password varchar(100));`;
            pool.query(userDetail, (error, result) => {
                if (error) {
                    console.log(error);

                }
                else {
                    const locationDetail = `CREATE TABLE ${vendorName}.locationDetail(id serial PRIMARY KEY, user_id integer REFERENCES ${vendorName}.userDetail(id), place varchar(50), pincode integer);`;
                    pool.query(locationDetail, (error, result) => {
                        if (error) {
                            console.log(error);
                        }
                        else {
                            console.log("location table created ");
                        }
                    })
                    const orderDetail = `CREATE TABLE ${vendorName}.orderDetail(id serial PRIMARY KEY, user_id integer REFERENCES ${vendorName}.userDetail(id), location_id integer REFERENCES ${vendorName}.locationDetail(id), total_price numeric);`;
                    pool.query(orderDetail, (error, result) => {
                        if (error) {
                            console.log(error);

                        }
                        else {
                            console.log("order table created");
                        }
                    })
                }
            })
            const catagoryDetail = `CREATE TABLE ${vendorName}.catagoryDetail(id integer PRIMARY KEY, catagory_name varchar(7));`;
            pool.query(catagoryDetail, (error, result) => {
                if (error) {
                    console.log(error);
                }
                else {
                    console.log("catagory table created");
                    const foodDetail = `CREATE TABLE ${vendorName}.foodDetail(id serial PRIMARY KEY, catagory_id integer REFERENCES ${vendorName}.catagoryDetail(id) ON DELETE CASCADE, food_name varchar(30), food_price numeric);`;
                    pool.query(foodDetail, (error, result) => {
                        if (error) {
                            console.log(error);
                        }
                        else {
                            console.log("food table created");

                            const cartItem = `CREATE TABLE ${vendorName}.cartItem(id serial PRIMARY KEY, food_id integer REFERENCES ${vendorName}.foodDetail(id), quantity integer, food_quantity_price numeric);`;
                            pool.query(cartItem, (error, result) => {
                                if (error) {
                                    console.log(error);
                                }
                                else {
                                    console.log("cart table created ");
                                }
                            })
                        }
                    })
                }
            })
        }
    })
}

vendorList = (req, res) => {
    pool.query(`SELECT s.nspname AS vendorname from pg_catalog.pg_namespace s 
                where nspname not in ('information_schema', 'pg_catalog','public')
                and nspname not like 'pg_toast%' and nspname not like 'pg_temp_%';`,
        (error, results) => {
            if (error) {
                res.status(500).json({
                    message: "Nothing to show",
                    error: error
                })
                throw error
            } else {
                res.status(200).json(results.rows)
            }
        })
}
deleteVendor = (req, res) => {
    const vendorName = req.query.vendor;
    console.log(req.query.vendor)
    pool.query(`DROP SCHEMA ${vendorName} CASCADE`, (error, results) => {
        if (error) {
            res.status(500).json({
                message: "Nothing to delete",
                error: error
            })
            throw error
        } else {
            res.status(201).json(`${vendorName} schema deleted`);
        }
    })
}
module.exports = {
    createVendor,
    vendorList,
    deleteVendor
}