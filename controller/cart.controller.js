const pool = require('./db')

addToCart = (req, res) => {
    const vendorName = req.params.vendor;
    const food_name = req.body.food_name;
    const quantity = req.body.quantity;
    pool.query(`insert into ${vendorName}.cartitem(food_id,quantity,food_quantity_price) values ((select id from ${vendorName}.fooddetail where food_name='${food_name}'),${quantity},(select food_price from ${vendorName}.fooddetail where food_name='${food_name}')*${quantity})`, (error, results) => {
        if (error) {
            console.log(error);
            res.status(500).json({
                message: "Data not inserted",
                error: error
            })
        } else {
            res.status(202).json(`food added`)
        }
    })
}

listCart = (req, res) => {
    const vendorName = req.params.vendor;
    pool.query(`SELECT food_id,quantity,food_quantity_price FROM ${vendorName}.cartItem`, (error, results) => {
        if (error) {
            res.status(204).json({
                message: "Nothing to display",
                error: error
            })
        } else {
            res.status(200).json(results.rows)
        }
    })
}

updateCartItem = (req, res) => {
    const vendorName = req.params.vendor;
    const id = req.params.id;
    const food_name = req.body.food_name;
    const quantity = req.body.quantity;
    pool.query(`UPDATE ${vendorName}.cartItem SET quantity=${quantity},food_quantity_price=((select food_price from ${vendorName}.fooddetail where food_name='${food_name}')*${quantity}) where id=${id}`, (error, results) => {
        if (error) {
            console.log(error)
            res.status(500).json({
                message: "data not updated",
                error: error
            })
        } else {
            res.status(200).json("data updated")
        }
    });
}

deleteCartItem = (req, res) => {
    const vendorName = req.params.vendor;
    const id = req.params.id;
    pool.query(`Delete from ${vendorName}.cartItem where id=${id} `, (error, results) => {
        if (error) {
            console.log(error)
            res.status(500).json({
                message: "data not deleted",
                error: error
            })
        } else {
            res.status(200).json("data deleted")
        }
    });
}

module.exports = {
    addToCart,
    listCart,
    updateCartItem,
    deleteCartItem
}