const pool = require('./db')

addCatagory = (req, res) => {
    const vendorName = req.params.vendor;
    const id = req.body.id;
    const catagory_name = req.body.catagory_name;
    // pool.query(`INSERT INTO ${vendorName}.catagoryDetail(id,catagory_name) VALUES (${id},'${catagory_name}');`, (error, results) => {
    //     if (error) {
    //         console.log(error);
    //         res.status(500).json({
    //             message: "Data not inserted",
    //             error: error
    //         })
    //     } else {
    //         res.status(202).json(`catagory added`)
    //     }
    // })
    pool.query(`(select id from ${vendorName}.catagoryDetail where id=${id});`, (error, results) => {
        if (results.rowCount == 0) {
            pool.query(`INSERT INTO ${vendorName}.catagoryDetail(id,catagory_name) VALUES (${id},'${catagory_name}');`, (error, results) => {
                if (error) {
                    console.log(error);
                    res.status(500).json({
                        message: "Data not inserted",
                        error: error
                    })
                } else {
                    res.status(200).json({
                        message: "success insert",
                        error: error
                    })
                }
            })
        }
        else {
            res.status(500).json(`catagory already exists`)
        }
    }
    )
}
listCatagory = (req, res) => {
    const vendorName = req.params.vendor;
    pool.query(`SELECT id,catagory_name FROM ${vendorName}.catagoryDetail`, (error, results) => {
        if (error) {
            res.status(204).json({
                message: "Nothing to display",
                error: error
            })
        } else {
            res.status(200).json(results.rows)
        }
    })
}
listSingleCatagory = (req, res) => {
    const vendorName = req.params.vendor;
    const id=req.params.id;
    pool.query(`SELECT id,catagory_name FROM ${vendorName}.catagoryDetail where id=${id};`, (error, results) => {
        if (error) {
            res.status(204).json({
                message: "Nothing to display",
                error: error
            })
        } else {
            res.status(200).json(results.rows)
        }
    })
}
updateCatagory = (req, res) => {
    const vendorName = req.params.vendor;
    const id = req.params.id;
    const catagory_name = req.body.catagory_name;
    pool.query(`UPDATE ${vendorName}.catagoryDetail SET catagory_name='${catagory_name}' where id=${id} `, (error, results) => {
        if (error) {
            console.log(error)
            res.status(500).json({
                message: "data not updated",
                error: error
            })
        } else {
            res.status(200).json("data updated")
        }
    });
}
deleteCatagory = (req, res) => {
    const vendorName = req.params.vendor;
    const id = req.params.id;
    pool.query(`Delete from ${vendorName}.catagoryDetail where id=${id} `, (error, results) => {
        if (error) {
            console.log(error)
            res.status(500).json({
                message: "data not deleted",
                error: error
            })
        } else {
            res.status(200).json("data deleted")
        }
    });
}
module.exports = {
    addCatagory,
    listCatagory,
    updateCatagory,
    deleteCatagory,
    listSingleCatagory
}