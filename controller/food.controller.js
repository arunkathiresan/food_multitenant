const pool = require('./db')

addFood = (req, res) => {
    const vendorName = req.params.vendor;
    const food_name = req.body.food_name;
    const food_price = req.body.food_price;
    const catagory_id=req.body.catagory_id;
    pool.query(`(select id from ${vendorName}.catagoryDetail where id=${catagory_id});`, (error, results) => {
        if (results.rowCount == 1) {
            pool.query(`INSERT INTO ${vendorName}.foodDetail(catagory_id,food_name,food_price) VALUES (${catagory_id},'${food_name}',${food_price});`, (error, results) => {
                if (error) {
                    console.log(error);
                    res.status(500).json({
                        message: "Data not inserted",
                        error: error
                    })
                } else {
                    res.status(200).json({
                        message: "success insert",
                        error: error
                    })
                }
            })
        }
        else {
            res.status(500).json(`Catagory not exists`)
        }
    }
    )
}
listFood = (req, res) => {
    const vendorName = req.params.vendor;
    pool.query(`SELECT id,catagory_id,food_name,food_price FROM ${vendorName}.foodDetail`, (error, results) => {
        if (error) {
            res.status(204).json({
                message: "Nothing to display",
                error: error
            })
        } else {
            res.status(200).json(results.rows)
        }
    })
}
listFoodId = (req, res) => {
    const vendorName = req.params.vendor;
    const id=req.params.id;
    pool.query(`SELECT id,catagory_id,food_name,food_price FROM ${vendorName}.foodDetail where id=${id}`, (error, results) => {
        if (error) {
            res.status(204).json({
                message: "Nothing to display",
                error: error
            })
        } else {
            res.status(200).json(results.rows)
        }
    })
}
listCatagoryFood=(req,res)=>{
    const vendorName=req.params.vendor;
    const catagory_id=req.params.catagory_id;
    pool.query(`SELECT food_name,food_price FROM ${vendorName}.foodDetail WHERE catagory_id=${catagory_id}`,(error,results)=>{
        if(error){
            console.log(error);
            res.status(204).json({
                message:"Nothing to display",
                error:error
            })
        }else{
            res.status(200).json(results.rows);
        }
    })
}

updateFood = (req, res) => {
    const vendorName = req.params.vendor;
    const id=req.params.id;
    const food_name = req.body.food_name;
    const food_price = req.body.food_price;
    const catagory_id=req.body.catagory_id;
    pool.query(`UPDATE ${vendorName}.foodDetail SET food_name='${food_name}', food_price=${food_price},catagory_id=${catagory_id} where id=${id}`, (error, results) => {
        if (error) {
            console.log(error)
            res.status(500).json({
                message: "data not updated",
                error: error
            })
        } else {
            res.status(200).json("data updated")
        }
    });
}
deleteFood = (req, res) => {
    const vendorName = req.params.vendor;
    const id = req.params.id;
    pool.query(`Delete from ${vendorName}.foodDetail where id=${id} `, (error, results) => {
        if (error) {
            console.log(error)
            res.status(500).json({
                message: "data not deleted",
                error: error
            })
        } else {
            res.status(200).json("data deleted")
        }
    });
}
module.exports = {
    addFood,
    listFood,
    listCatagoryFood,
    updateFood,
    deleteFood,
    listFoodId
}